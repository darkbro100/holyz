package me.mario.ffa;

import static org.bukkit.ChatColor.AQUA;
import static org.bukkit.ChatColor.BLUE;
import static org.bukkit.ChatColor.DARK_AQUA;
import static org.bukkit.ChatColor.DARK_BLUE;
import static org.bukkit.ChatColor.DARK_GREEN;
import static org.bukkit.ChatColor.DARK_PURPLE;
import static org.bukkit.ChatColor.DARK_RED;
import static org.bukkit.ChatColor.GOLD;
import static org.bukkit.ChatColor.GRAY;
import static org.bukkit.ChatColor.GREEN;
import static org.bukkit.ChatColor.LIGHT_PURPLE;
import static org.bukkit.ChatColor.RED;
import static org.bukkit.ChatColor.YELLOW;

import java.util.HashMap;
import java.util.function.Function;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.brawl.base.BrawlPlayer;
import com.brawl.base.util.scheduler.Async;

/**
 * @author redslime
 * @version 2018-12-14
 */
public class DisplaynameFormatter {

    private HashMap<ChatColor, String> teams = new HashMap<>();

    private ChatColor[] colors = { RED, GOLD, YELLOW, GREEN, BLUE, LIGHT_PURPLE, DARK_PURPLE, DARK_RED, DARK_GREEN, DARK_BLUE, DARK_AQUA, AQUA, GRAY };
    
    public DisplaynameFormatter() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        Function<Integer, String> pos = i -> String.valueOf(alphabet.charAt(25-i));

        for(int i = 0; i < colors.length; i++) {
        	ChatColor c = colors[i];
            teams.put(c, pos.apply(i) + c.name());
        }

        Async.get().interval(1).run(this::update);
    }

    private void update() {
        try {
            for(Player observer : Bukkit.getOnlinePlayers()) {
                for(Player target : Bukkit.getOnlinePlayers()) {
                	GamePlayer gp = GamePlayer.get(target);
                	ChatColor color = ChatColor.GRAY;
                	
                	if(gp.getTeam() != null) {
                		color = gp.getTeam().color;
                	}
                	String displayName = BrawlPlayer.of(target).getDisplayName();
                	
                    Team team = getTeam(observer, target, color);
                    if(!isInTeam(observer, target, color)) {
                        leaveTeam(observer, target);
                        team.addEntry(displayName);
                    } else {
                    	updateTeam(team, color);
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTeam(Team team, ChatColor color) {
    	String prefix = color.toString();
        
        if(team.getPrefix() == null) {
        	team.setPrefix(prefix);
        } else if(team.getPrefix() != null && !team.getPrefix().equals(prefix)) {
        	team.setPrefix(prefix);
        }
    }
    
    private String getTeamName(ChatColor c, String targetName) {
    	String name = teams.get(c) + targetName;
    	if(name.length() > 16) {
    		name = name.substring(0, 16);
    	}
    	
    	return name;
    }
    
    private Team getTeam(Player player, Player target, ChatColor color) {
        Scoreboard sc = player.getScoreboard();
    	String teamName = getTeamName(color, BrawlPlayer.of(target).getDisplayName());
        Team team = sc.getTeam(teamName);
        if(team == null) {
            team = sc.registerNewTeam(teamName);
            team.setAllowFriendlyFire(true);
            team.setCanSeeFriendlyInvisibles(false);
            team.setPrefix(color.toString());
        }
        
        return team;
    }

    private void leaveTeam(Player observer, Player target) {
    	String entry = BrawlPlayer.of(target).getDisplayName();
    	if(entry == null || entry.isEmpty()) {
    		entry = target.getName();
    	}
    	
        Team previous = observer.getScoreboard().getEntryTeam(entry);
        if(previous != null)
            previous.removeEntry(entry);
    }

    private boolean isInTeam(Player observer, Player target, ChatColor color) {
    	String teamName = getTeamName(color, BrawlPlayer.of(target).getDisplayName());
        return observer.getScoreboard().getTeam(teamName).hasEntry(target.getName());
    }
}
