package me.mario.ffa.util;

import java.io.File;
import java.io.IOException;

import me.mario.ffa.GameSign;
import me.mario.ffa.Main;
import me.mario.ffa.Map;
import me.mario.ffa.SignType;

import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;

public class FileHandler {

	private static YamlConfiguration config;
	private static File cfile;
	
	public FileHandler(Main plugin) {

		if (!plugin.getDataFolder().exists())
			plugin.getDataFolder().mkdir();

		File players = new File("/Shared/joecraft/event/server/plugins/ffa/data");
		if (!players.exists())
			players.mkdir();
		
		cfile = new File(plugin.getDataFolder(), "config.yml");
		
		if(!cfile.exists()) {
			try {
				config = YamlConfiguration.loadConfiguration(cfile);
				
				cfile.createNewFile();
				setDefaults();
			}catch(Exception e) { }
		}
		
		config = YamlConfiguration.loadConfiguration(cfile);
	}
	
	public void setDefaults() {
		config.set("ip", "ip");
		config.set("user", "ip");
		config.set("pass", "ip");
		config.set("port", "ip");
		config.set("database", "ip");
		
		try {
			config.save(cfile);
		}catch(Exception e) { }
	}
	
	public String getIp (){
		System.out.println(config.getString("ip"));
		return config.getString("ip");
	}
	
	public String getUser() {
		System.out.println(config.getString("user"));
		return config.getString("user");
	}
	
	public String getPassword() {
		System.out.println(config.getString("pass"));
		return config.getString("pass");
	}
	
	public String getDB() {
		System.out.println(config.getString("database"));
		return config.getString("database");
	}
	
	public int getPort() {
		System.out.println(config.getInt("port"));
		return config.getInt("port");
	}
	
	public void saveMap(Map map) throws IOException {
		Location loc1 = map.getSelection().getMinimumPoint();
		Location lo2 = map.getSelection().getMaximumPoint();
		Location lobby = map.getLobbyLoc();
		String name = map.getName();
		
		File file = new File(Main.getInstance().getDataFolder() + "/maps");
		if(!file.exists()) file.mkdir();
		
		File mapfile = new File(file, name + ".yml");
		if(!mapfile.exists()) mapfile.createNewFile();
		
		YamlConfiguration config = YamlConfiguration.loadConfiguration(mapfile);
		
		config.set("name", name);
		config.set("lobby", LocUtil.locToString(lobby));
		config.set("minpoint", LocUtil.locToString(loc1));
		config.set("maxpoint", LocUtil.locToString(lo2));
		
		try {
			config.save(mapfile);
		}catch(Exception e) { }
	}
	
	public Map loadMap(File file) {
		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
		
		String name = config.getString("name");
		Location lobby = LocUtil.locFromString(config.getString("lobby"));
		Location minp = LocUtil.locFromString(config.getString("minpoint"));
		Location maxp = LocUtil.locFromString(config.getString("maxpoint"));
		
		CuboidSelection sel = new CuboidSelection(minp.getWorld(), minp, maxp);
		
		return new Map(name, sel, lobby);
	}

	public void loadMaps() {
		File file = new File(Main.getInstance().getDataFolder() + "/maps");
		if(!file.exists()) file.mkdir();
		
		for(File mapfile : file.listFiles()) {
			loadMap(mapfile);
		}
	}

	public void saveSign(GameSign sign) {
		File signs = new File(Main.getInstance().getDataFolder(), "signs.yml");
		if(!signs.exists()) {
			try {
				signs.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		YamlConfiguration config = YamlConfiguration.loadConfiguration(signs);
		
		config.set("signs." + sign.getName() + ".type", sign.getType().name());
		config.set("signs." + sign.getName() + ".number", sign.getNumber());
		config.set("signs." + sign.getName() + ".loc", LocUtil.locToString(sign.getLocation()));
		
		try {
			Main.getInstance().saveResource(signs.getName(), false);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void loadSigns() {
		File signs = new File(Main.getInstance().getDataFolder(), "signs.yml");
		if(!signs.exists()) {
			try {
				signs.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		YamlConfiguration config = YamlConfiguration.loadConfiguration(signs);
		
		if(config.getConfigurationSection("signs") != null) {
			for(String name : config.getConfigurationSection("signs").getKeys(false)) {
				Location loc = LocUtil.locFromString(config.getString("signs." + name + ".loc"));
				SignType type = SignType.valueOf(config.getString("signs." + name + ".type"));
				int number = config.getInt("signs." + name + ".number");
				
				new GameSign(loc, type, number, name);
			}
		}
	}

	public String convert(String n) {
		int num = Integer.valueOf(n);
		
		int totalMinutes = num / 60;
		int minutes = totalMinutes % 60;
		int totalHours = totalMinutes / 60;
		int hours = totalHours % 24;
		int days = totalHours / 24;
		
		String d = (days <= 0 ? "" : days + "d");
		String h = (hours <= 0 ? "" : hours + "h");
		String m = (minutes <= 0 ? "" : minutes + "m");
		
		return new String(d + h + m);
	}
}
