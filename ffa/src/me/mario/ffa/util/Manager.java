package me.mario.ffa.util;

import java.util.ArrayList;

public class Manager
{
  public ArrayList<String> getTrimmed(String total)
  {
    return trim(total);
  }

  private ArrayList<String> trim(String untrimmed)
  {
    ArrayList<String> finals = new ArrayList<String>();
    String temp = "";
    for (int i = 1; i < untrimmed.length(); i++) {
      if ((untrimmed.charAt(i) == ',') || (i == untrimmed.length() - 1)) {
        finals.add(temp);
        i++;
        temp = "";
      } else if (untrimmed.charAt(i) == '=') {
        temp = temp + " = ";
      } else {
        temp = temp + untrimmed.charAt(i);
      }
    }
    return finals;
  }
}