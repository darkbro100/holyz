package me.mario.ffa.util;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.google.common.collect.Lists;

import me.mario.ffa.GamePlayer;
import me.mario.ffa.GameTask;
import me.mario.ffa.Main;

public class ScoreboardUtil {
	
	public static final String SPACER = "{s}";

	public static Scoreboard scoreboard(Player player) {
		GamePlayer gp = GamePlayer.get(player);
		Scoreboard sb = gp.getScoreboard();
		
		Objective statsObj;
		if(sb.getObjective("stats") == null) {
			statsObj = sb.registerNewObjective("stats", "dummy");
			statsObj.setDisplaySlot(DisplaySlot.SIDEBAR);
			statsObj.setDisplayName(ChatColor.RED + "Brawl");
		} else {
			statsObj = sb.getObjective("stats");
		}
		
		Objective healthObj;
		if(sb.getObjective("healthobj") == null) {
			healthObj = sb.registerNewObjective("healthobj", "dummy");
			healthObj.setDisplaySlot(DisplaySlot.BELOW_NAME);
			healthObj.setDisplayName(ChatColor.RED + "❤");
		} else {
			healthObj = sb.getObjective("healthobj");
		}
		
		List<String> scores = Lists.newArrayList();
		
		if(!Main.game.isStarted()) {
			scores.add("Starting: " + GameTask.timeUntilEnds);
		} else {
			scores.add("Team: " + (gp.getTeam() == null ? "\2478none" : gp.getTeam().color + gp.getTeam().name));
//			for(Team team : Main.game.getTeams()) {
//				ChatColor color = team.color;
//				
//				if(sb.getTeam(team.name) == null) {
//					org.bukkit.scoreboard.Team t = sb.registerNewTeam(team.name);
//					t.setPrefix(color.toString());
//				}
//				org.bukkit.scoreboard.Team steam = sb.registerNewTeam(team.name);
//				steam.setPrefix(color + "");
//				
//				for(GamePlayer member : team.getMembers()) {
//					steam.addPlayer(member.getPlayer());
//				}
//			}
		}
		
		scores.add(SPACER);
		scores.add("Played: \247e" + gp.gamesplayed);
		scores.add("\247a" + gp.wins + " \247rWon");
		scores.add("\247c" + (gp.gamesplayed - gp.wins) + "\247r Lost");
		scores.add(SPACER);
		scores.add("Kills: \247a" + gp.kills);
		scores.add("Deaths: \247c" + gp.deaths);
		scores.add("ELO: \247a" + gp.elo);
		scores.add(SPACER);
		scores.add("Game Kills: \247a" + gp.gamekills);
		scores.add("Killstreak: \247c" + gp.gamekillstreak);
		
		resetScores(sb);
		addScores(statsObj, scores);
		
		//Ugly as fuck but whatever
		for(Player p : Bukkit.getOnlinePlayers()) {
			int score = (int) p.getHealth();
			Score s = healthObj.getScore(p.getName());
			s.setScore(score);
		}
		
		return sb;
	}
	
	/**
	 * Adds all the given entries to the given Objective in order, where the
	 * first argument is the top of the Scoreboard and the last argument is the
	 * bottom of the Scoreboard.
	 */
	public static void addScores(Objective objective, List<String> entries) {
		String lastSpace = "";
		for (int i = 0; i < entries.size(); i++) {
			String entry = entries.get(i);
			if (entry.equals(SPACER))
				entry = (lastSpace += " ");
			objective.getScore(entry).setScore(entries.size() - i);
		}
	}
	
	/**
	 * Resets the score of every entry in the given Scoreboard.
	 */
	public static void resetScores(Scoreboard scoreboard) {
		scoreboard.getEntries().forEach(scoreboard::resetScores);
	}
	
}
