	package me.mario.ffa.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocUtil {

	public static Location locFromString(String string) {
		String[] array = string.split(",");

		Location loc = new Location(Bukkit.getWorld(array[0]),
				Double.valueOf(array[1]), Double.valueOf(array[2]),
				Double.valueOf(array[3]));
		return loc;
	}

	public static String locToString(Location loc) {
		String string = loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ();
		
		return string;
	}
	
}
