package me.mario.ffa.util;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Comp
  implements Comparator<Object>
{
  Map<String, Integer> base;
  Map<String, Double> base2;
  
  private boolean use;
  
  public Comp(Map<String, Integer> base)
  {
	  use = true;
    this.base = base;
  }
  
  public Comp(HashMap<String, Double> base)
  {
	use = false;
    this.base2 = base;
  }

  public int compare(Object a, Object b) {
	  if(use) {
		    if (this.base.get(a) != null) {
		        if (this.base.get(a).intValue() < (this.base.get(b)).intValue())
		          return 1;
		        if (this.base.get(a) == this.base.get(b)) {
		          return 1;
		        }
		        return -1;
		      }  
	  } else {
		    if (this.base2.get(a) != null) {
		        if (this.base2.get(a).doubleValue() < (this.base2.get(b)).doubleValue())
		          return 1;
		        if (this.base2.get(a) == this.base2.get(b)) {
		          return 1;
		        }
		        return -1;
		      }  
	  }

    return 0;
  }
}