package me.mario.ffa.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.mario.ffa.GameClass;
import me.mario.ffa.GamePlayer;
import me.mario.ffa.Main;

public class DBUtil {

	private static Connection connection;
	
	private String ip, user, password, database;
	private int port;
	
	public DBUtil(String ip, int port, String user, String password, String database) {
		this.ip = ip;
		this.user = user;
		this.port = port;
		this.password = password;
		this.database = database;
	}
	
	public Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://" + ip + "/"
					+ database + "?user=" + user + "&password=" + password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	public void closeConnection() {
		if(connection != null)
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	public void createStatement(String query) {
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean contains(UUID uuid) {
		boolean b = false;
		try {
			PreparedStatement statement = connection.prepareStatement("select * from gunz_stats where uuid=?");
			statement.setString(1, uuid.toString());
			
			ResultSet set = statement.executeQuery();
			
			b = set.next();
			
			statement.close();
			set.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return b;
	}
	
	public Object getPlayerInformation(UUID uuid, String searchingFor) {
		PreparedStatement statement;
		Object toReturn = null;
		try {
			statement = connection.prepareStatement("select " + searchingFor + " from gunz_stats where uuid=?");
			statement.setString(1, uuid.toString());
			
			ResultSet set = statement.executeQuery();
			
			if(set.next()) {
				toReturn = set.getObject(searchingFor);
			}
			
			set.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			closeConnection();
		}
		return toReturn;
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public void load(GamePlayer player) {
		if(contains(player.getPlayer().getUniqueId())) {
			player.kills = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "kills");
			player.wins = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "wins");
			player.gamesplayed = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "gamesPlayed");
			player.deaths = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "deaths");
			
			player.isAdmin = (boolean) getPlayerInformation(player.getPlayer().getUniqueId(), "admin");
			System.out.println(player.isAdmin);
			
			player.elo = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "elo");
			player.bkillstreak = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "bestKillstreak");
			player.bgamekills = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "bestGameKills");
			player.timePlayed = (int) getPlayerInformation(player.getPlayer().getUniqueId(), "timePlayed");
			
			player.gclass = new GameClass(ItemSerialization.StringToInventory((String) getPlayerInformation(player.getPlayer().getUniqueId(), "class")).getContents());
			ItemStack[] realClass = new ItemStack[player.gclass.getContents().length];
			
			for(int i = 0; i < player.gclass.getContents().length; i++) {
				ItemStack item = player.gclass.getContents()[i];
				if(item == null) continue;
				if(item.getType() == null) continue;
				
				if(!GameClass.validClassItem(item)) {
					realClass[i] = new ItemStack(Material.AIR);
				} else {
					realClass[i] = item;
				}
			}
			
			player.gclass = new GameClass(realClass);
		} else {
			player.kills = 0;
			player.wins = 0;
			player.gamesplayed = 0;
			player.deaths = 0;
			player.gclass = GameClass.defaultClass();
		}
	}
	
	public void save(GamePlayer player) {
		if(contains(player.getPlayer().getUniqueId())) {
			createStatement(String
					.format("update gunz_stats set kills=%s, deaths=%s, wins=%s, gamesPlayed=%s, admin=%s, bestKillstreak=%s, bestGameKills=%s, timePlayed=%s, name='%s', elo=%s, class='%s' where uuid='%s'",
							player.kills, player.deaths, player.wins,
							player.gamesplayed, player.isAdmin,
							player.bkillstreak, player.bgamekills,
							player.timePlayed, player.getPlayer().getName(),
							player.elo, ItemSerialization
									.InventoryToString(player.gclass.getInventory()), player.getPlayer().getUniqueId().toString()));
		} else {
			createStatement(String
					.format("insert into gunz_stats (uuid,name,timePlayed,kills,deaths,wins,gamesPlayed,admin,bestKillstreak,bestGameKills,elo,class)values('%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s')",
							player.getPlayer().getUniqueId(), player.getPlayer().getName(), player.timePlayed, player.kills, player.deaths, player.wins, player.gamesplayed, player.isAdmin, player.bkillstreak, player.bgamekills, player.elo, ItemSerialization.InventoryToString(player.gclass.getInventory())));
		}
	}
	
	public static synchronized ArrayList<String> getTop10(String object) {
		ArrayList<String> toReturn = new ArrayList<String>();
		Main.db.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT name," + object + " FROM gunz_stats ORDER BY " + object + " DESC LIMIT 10;");
			ResultSet set = statement.executeQuery();
			
			while(set.next()) {
				toReturn.add(set.getString("name") + " = " + set.getObject(object) + " " + object);
			}
			Main.db.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return toReturn;
	}
	
	public static synchronized ArrayList<String> getBestKDR() {
		ArrayList<String> toReturn = new ArrayList<String>();
		double bestKdr = 0;
		Main.db.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT name,kills,deaths FROM gunz_stats");
			ResultSet set = statement.executeQuery();
			
			while(set.next()) {
				double kills = (double) set.getInt("kills");
				double deaths = (double) set.getInt("deaths");
				double kdr = kills / (deaths == 0 ? 1 : deaths);
				if(kdr > bestKdr) {
					bestKdr = kdr;
					if(toReturn.size() < 1) {
						toReturn.add(set.getString("name") + " = " + new DecimalFormat("##.##").format(kdr) + " " + "KDR");
					}
					else {
						toReturn.set(0, set.getString("name") + " = " + new DecimalFormat("##.##").format(kdr) + " " + "KDR");
					}
				}
			}
			Main.db.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return toReturn;
	}
}