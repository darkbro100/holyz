package me.mario.ffa;

import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.brawl.base.command.Command;

public class StatsCommand extends Command {

	public StatsCommand() {
		super("stats");
	}
	
	@Override
	public boolean onExecute(CommandSender sender, String alias, String[] args) {
		if(args.length == 0) {
			sender.sendMessage("/stats <name>");
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target != null) {
			GamePlayer gp = GamePlayer.get(target);
			
			sender.sendMessage("\247dStats of: \247e" + gp.getPlayer().getName());
			sender.sendMessage("");
			sender.sendMessage("ELO: \247e" + gp.elo);
			sender.sendMessage("Kills: \247e" + gp.kills);
			sender.sendMessage("Deaths: \247e" + gp.deaths);
			double kills = gp.kills;
			double deaths = gp.deaths;
			double kdr = kills / deaths;
			DecimalFormat df = new DecimalFormat("##.##");
			sender.sendMessage("KDR: \247e" + df.format(kdr));
			sender.sendMessage("Games Played: \247e" + gp.gamesplayed);
			sender.sendMessage("Games Won: \247e" + gp.wins);
			sender.sendMessage("Games Lost: \247e" + (gp.gamesplayed - gp.wins));				
		} else {
			sender.sendMessage("Player not online");
		}
		
		return true;
	}

}
