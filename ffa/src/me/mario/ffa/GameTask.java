package me.mario.ffa;

import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;


public class GameTask extends BukkitRunnable {

	public static int timeUntilEnds = 30;
	public static int timeUntilMatchEnds = 300;
	
	public void run() {
		if(Main.game.stopped) return;
		
		if(GamePlayer.players.size() >= 2 && !Main.game.isStarted()) {
			if(timeUntilEnds <= 0) {
				Main.game.start();
				timeUntilEnds = 30;
			}
			
			timeUntilEnds--;
			
			if(timeUntilEnds <= 10 || timeUntilEnds == 15) {
				Main.broadcast(timeUntilEnds + " seconds left until start");
			}
		}
		
		if(Main.game.isStarted()) {
			if(timeUntilMatchEnds <= 0) {
				Main.game.end();
				timeUntilMatchEnds = 300;
			}
			
			timeUntilMatchEnds--;
			
			if(timeUntilMatchEnds <= 10 || timeUntilMatchEnds == 15 || timeUntilMatchEnds == 30 || timeUntilMatchEnds == 60 || timeUntilMatchEnds == 120) {
				Main.broadcast(timeUntilMatchEnds + " seconds left until game ends");
			}
		}
		
		for(GamePlayer player : GamePlayer.players) {
			player.timePlayed++;
			if(Main.game.isStarted()) {
				player.getPlayer().setCompassTarget(player.nearestPlayer());
				
				if(player.getPlayer().getLocation().getBlock().getType() == Material.WEB) {
					player.getPlayer().damage(4D);
				}
			}
		}
		
		if(Main.game.isStarted()) {
			Main.game.getCurrentMap().getLobbyLoc().getWorld().setTime(6000);
			Main.game.getCurrentMap().getLobbyLoc().getWorld().setWeatherDuration(0);
		}
	}
	
}
