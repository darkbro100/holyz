package me.mario.ffa;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;

import com.brawl.base.util.scheduler.Sync;
import com.google.common.collect.Lists;

import me.mario.ffa.Game.GameType;
import me.mario.ffa.util.ScoreboardUtil;
import me.signatured.shared.scheduler.TaskHolder;
import net.minecraft.server.v1_8_R3.GenericAttributes;

public class GamePlayer {

	public static ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();

	private Player player;
	public boolean isInGame = false, hasdogs = false, isAdmin = false;
	public int kills, deaths, gamekills = 0, gamekillstreak = 0, gamesplayed,
			wins, elo = 100, bkillstreak = 0, bgamekills = 0, timePlayed = 0;
	private Team team;
	
	public ArrayList<LivingEntity> dogs = new ArrayList<LivingEntity>();
	public HashMap<String, Integer> killedMost = new HashMap<String, Integer>();
	public HashMap<String, Integer> diedMost = new HashMap<String, Integer>();
	public List<Entity> recentAttackers = Lists.newArrayList();
	
	public GameClass gclass;

	public File file;

	private boolean spectating = false, canKill = true;

	public boolean godmoded = false;

	public boolean modifyingClass = false;

	public boolean selectingItem = false;

	public int slot = 0;

	public int clickspersecond = 0;

	public boolean vanished = false;

	public boolean hasheli = false;

	public LivingEntity helicopter = null;

	public int flags = 0;

	public boolean helicoptercooldown = false;

	private Scoreboard scoreboard;
	
	public TaskHolder scoreboardTask;
	
	public GamePlayer(Player player) {
		this.player = player;
		Main.db.load(this);
		players.add(this);
		PermissionAttachment attach = this.player.addAttachment(Main.getInstance());
		attach.setPermission("grapplinghook.pull.self", true);
		attach.unsetPermission("grapplinghook.pull.players");
		attach.unsetPermission("bukkit.command.me");
		attach.unsetPermission("bukkit.command.help");
		attach.unsetPermission("bukkit.command.plugins");
		
		this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		this.player.setScoreboard(this.scoreboard);
		
		this.scoreboardTask = new TaskHolder();
		Sync.get().interval(10).holder(scoreboardTask).run(() -> ScoreboardUtil.scoreboard(this.player));
	}

	public boolean isInGame() {
		return isInGame;
	}

	public GameClass getGameClass() {
		return gclass;
	}

	public File getFile() {
		return file;
	}

	public Player getPlayer() {
		return player;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public void giveKit() {
		player.getInventory().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
		player.getInventory().setChestplate(
				new ItemStack(Material.IRON_CHESTPLATE));
		player.getInventory().setLeggings(
				new ItemStack(Material.DIAMOND_LEGGINGS));
		player.getInventory().setBoots(new ItemStack(Material.DIAMOND_BOOTS));

		player.getInventory().setContents(gclass.getContents());

		player.getInventory().addItem(new ItemStack(Material.CLAY_BALL, 196));
		player.getInventory().addItem(new ItemStack(Material.FLINT, 196));
		player.getInventory().addItem(new ItemStack(Material.SEEDS, 196));
		player.getInventory().addItem(new ItemStack(Material.PUMPKIN_SEEDS, 196));
		player.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 64));
		
		player.updateInventory();
	}

	public static GamePlayer get(Player player2) {
		for (GamePlayer player : players) {
			if (player.getPlayer().getName()
					.equalsIgnoreCase(player2.getName()))
				return player;
		}
		return null;
	}

	public static void remove(GamePlayer player2) {
		Main.db.getConnection();
		Main.db.save(player2);
		Main.db.closeConnection();
		player2.setSpectating(false);
		player2.setHasDogs(false);
		player2.setHasHelicopter(false);
		player2.scoreboardTask.cancel();
		
		players.remove(player2);
	}

	public void clear() {
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
	}

	public void teleport() {
		player.teleport(Map.getRandomMap().getLobbyLoc());
	}

	public void addWin() {
		wins++;
	}

	public void addKill(GamePlayer dead) {
		if(canKill) {
			player.getInventory().addItem(new ItemStack(Material.FLINT, 32));
			player.getInventory().addItem(new ItemStack(Material.CLAY_BALL, 16));
			player.getInventory().addItem(new ItemStack(Material.SEEDS, 16));
			player.getInventory().addItem(new ItemStack(Material.PUMPKIN_SEEDS, 16));
			player.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 8));

			player.getInventory().addItem(new ItemStack(Material.INK_SACK, 10, (byte)11));
			
			kills++;
			addGameKill();
			canKill = false;
			
			int newelo = (int) (dead.elo * 0.02);
			
			this.elo += newelo;
			dead.elo -= newelo;
			
			if(getTeam() != null) getTeam().addKill();
			
			if(killedMost.containsKey(dead.getPlayer().getName())) {
				int newKills = killedMost.get(dead.getPlayer().getName()) + 1;
				killedMost.put(dead.getPlayer().getName(), newKills);
			}
			else {
				killedMost.put(dead.getPlayer().getName(), 1);
			}
			
			
			if(dead.diedMost.containsKey(getPlayer().getName())) {
				int newDeaths = dead.diedMost.get(getPlayer().getName()) + 1;
				dead.diedMost.put(getPlayer().getName(), newDeaths);
			}
			else {
				dead.diedMost.put(getPlayer().getName(), 1);
			}
			
			Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				public void run() {
					canKill = true;
				}
			}, 10L);
		}
	}
	
	public void addNukeKill(GamePlayer dead) {
			player.getInventory().addItem(new ItemStack(Material.FLINT, 32));
			player.getInventory().addItem(new ItemStack(Material.CLAY_BALL, 16));
			player.getInventory().addItem(new ItemStack(Material.SEEDS, 16));
			
			player.getInventory().addItem(new ItemStack(Material.INK_SACK, 10, (byte)11));
			
			int newelo = (int) (dead.elo * 0.02);
			
			this.elo += newelo;
			dead.elo -= newelo;
			
			if(killedMost.containsKey(dead.getPlayer().getName())) {
				System.out.println("put for killed most");
				int newKills = killedMost.get(dead.getPlayer().getName()) + 1;
				killedMost.put(dead.getPlayer().getName(), newKills);
			}
			else {
				System.out.println("put first index for killed most");
				killedMost.put(dead.getPlayer().getName(), 1);
			}
			
			
			if(dead.diedMost.containsKey(getPlayer().getName())) {
				int newDeaths = dead.diedMost.get(getPlayer().getName()) + 1;
				dead.diedMost.put(getPlayer().getName(), newDeaths);
			}
			else {
				dead.diedMost.put(getPlayer().getName(), 1);
			}
			
			kills++;
			addGameKill();
	}

	public void addGameKill() {
		gamekillstreak++;
		if(gamekillstreak > bkillstreak) {
			bkillstreak = gamekillstreak;
		}
		
		gamekills++;
		if(gamekills > bgamekills) {
			bgamekills = gamekills;
		}
		
		checkForSpecial();
	}

	private void checkForSpecial() {
		final GamePlayer self = this;
		if (gamekillstreak == 2) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,
					2400, 1));
			for (GamePlayer player : Main.game.getPlayers()) {
				player.player.sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained the speed boost!");
			}
		} else if (gamekillstreak == 4) {
			player.getInventory().setChestplate(
					new ItemStack(Material.DIAMOND_CHESTPLATE));
			for (GamePlayer player : Main.game.getPlayers()) {
				player.player.sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained full diamond!");
			}
		} else if(gamekillstreak == 6) {
			player.getInventory().addItem(new ItemStack(Material.GOLD_RECORD));
			for(GamePlayer player : Main.game.getPlayers()) {
				player.getPlayer().sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained a minigun!");
			}
		} else if(gamekillstreak == 7) {
			player.getInventory().addItem(new ItemStack(Material.BROWN_MUSHROOM, 5));
			for(GamePlayer player : Main.game.getPlayers()) {
				player.getPlayer().sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained claymores!");
			}
		} else if(gamekillstreak == 8) {
			player.getInventory().addItem(new ItemStack(Material.NETHER_STAR));
			for(GamePlayer player : Main.game.getPlayers()) {
				player.getPlayer().sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained a carepackage!");
			}
		} else if(gamekillstreak == 10) {
			for(GamePlayer player : Main.game.getPlayers()) {
				player.getPlayer().sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained \247cDOGS!");
			}
			
			self.setHasDogs(true);
		} else if(gamekillstreak == 15) {
			for(GamePlayer player : Main.game.getPlayers()) {
				player.getPlayer().sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained an \2474ATTACK HELICOPTER!");
			}
			
			self.setHasHelicopter(true);
		} else if(gamekillstreak == 25) {	
			for(final GamePlayer player : Main.game.getPlayers()) {
				if(player == self) {
					self.getPlayer().sendMessage("\2477YOU OBTAINED A NUKE!!!");
					continue;
				}
				
				player.getPlayer().sendMessage("\247a" + this.player.getName()
						+ "\2477 has obtained a nuke! ITS ALL OVER!!!!!!!!!!");
				
				Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
					public void run() {
						if(Main.game.isStarted()) {
							if(Main.game.getGameType() != GameType.FFA) {
								if(!self.getTeam().name.equalsIgnoreCase(player.getTeam().name)) {
									player.getPlayer().setHealth(0);
									self.addNukeKill(player);
									self.getPlayer().sendMessage("\247aKilled: \2477" + player.getPlayer().getName());
								}
							} else {
								player.getPlayer().setHealth(0);
								self.addNukeKill(player);
								self.getPlayer().sendMessage("\247aKilled: \2477" + player.getPlayer().getName());
							}
						}
					}
				}, 20L * 5L);
			}
		}
	}

	public void setHasDogs(boolean b) {
		hasdogs = b;
		
		if(hasdogs) {
			for(int i = 0; i < 5; i++) {
				Entity ent = getPlayer().getWorld().spawnEntity(getPlayer().getLocation(), EntityType.WOLF);
				LivingEntity lent = (LivingEntity) ent;
				
				lent.setCustomName(getPlayer().getName() + "'s Dog");
				lent.setCustomNameVisible(false);
				((CraftLivingEntity)lent).getHandle().getAttributeInstance(GenericAttributes.maxHealth).setValue(500D);
				lent.setMaxHealth(500D);
				lent.setHealth(500D);
				lent.setHealth(lent.getMaxHealth());
				
				lent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,
						999999999, 3));
				
				dogs.add(lent);
			}
		}
	}
	
	public void setHasHelicopter(boolean b) {
		hasheli = b;
		
		if(hasheli) {
			Entity ent = getPlayer().getWorld().spawnEntity(getPlayer().getLocation().add(0, 20, 0), EntityType.GHAST);
			LivingEntity lent = (LivingEntity) ent;
			((CraftLivingEntity) lent).getHandle().getAttributeInstance(GenericAttributes.maxHealth)
			.setValue(2500);
			((CraftLivingEntity) lent).getHandle().getAttributeInstance(GenericAttributes.MOVEMENT_SPEED)
			.setValue(2.5);
			lent.setMaxHealth(2500D);
			lent.setCustomName(getPlayer().getName() + "'s Attack Helicopter");
			lent.setCustomNameVisible(true);
			lent.setHealth(lent.getMaxHealth());
			lent.setHealth(2500D);
			lent.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,
					999999999, 3));
			
			helicopter = lent;
		} else {
			if(helicopter != null) helicopter.remove();
			helicopter = null;
		}
	}

	public void addGamesPlayed() {
		gamesplayed++;
	}

	public void resetGameStats() {
		gamekills = 0;
		gamekillstreak = 0;
		player.removePotionEffect(PotionEffectType.SPEED);
		diedMost.clear();
		killedMost.clear();
	}

	public void resetKillstreak() {
		gamekillstreak = 0;
	}

	public void addDeath() {
		deaths++;
	}

	public Location nearestPlayer() {
		double closest = 5555555555555555D;
		Player closestp = null;

		for (Entity ent : player.getNearbyEntities(175D, 175D, 175D)) {
			if (ent instanceof Player) {
				if (ent.getLocation().distance(player.getLocation()) < closest) {
					GamePlayer enm = GamePlayer.get((Player) ent);
					if (!enm.isInGame())
						continue;

					if (enm.getTeam() != null && enm.getTeam().equals(team))
						continue;
					closest = ent.getLocation().distance(player.getLocation());
					closestp = (Player) ent;
				}
			}
		}

		if (closestp == null)
			return player.getLocation();

		return closestp.getLocation();
	}

	public void setSpectating(boolean spectating) {
		this.spectating = spectating;

		if (isSpectating()) {
			for (GamePlayer oplayer : players) {
				if (oplayer.isInGame && !oplayer.isAdmin) {
					oplayer.getPlayer().hidePlayer(player);
					player.teleport(oplayer.getPlayer());
				}
			}

			player.setAllowFlight(true);
			player.setFlying(true);

			clear();
			
			setGodMode(true);
			player.setGameMode(GameMode.SPECTATOR);
			
			player.sendMessage("You are now spectating, tp to players by doing /tp <name>");
		} else {
			player.teleport(Map.getRandomMap().getLobbyLoc());
			for (GamePlayer oplayer : players) {
				oplayer.getPlayer().showPlayer(player);
			}

			player.setAllowFlight(false);
			player.setFlying(false);
			
			player.setGameMode(GameMode.SURVIVAL);
			
			clear();
			
			setGodMode(false);
		}
	}

	public void setGodMode(boolean b) {
		this.godmoded = b;
	}

	public boolean isSpectating() {
		return spectating;
	}
	
	public String getKilledMost() {
		String found = null;
		int mostKills = 0;
		
		for(Entry<String, Integer> entry : killedMost.entrySet()) {
			if(entry.getValue() > mostKills) {
				found = entry.getKey();
				mostKills = entry.getValue();
			}
		}
		
		if(found == null) {
			return "You did not kill anyone that match!";
		}
		
		return "You killed " + found + " the most by killing them " + mostKills + " times!";
	}
	
	public String getDiedMost() {
		String found = null;
		int mostKills = 0;
		
		for(Entry<String, Integer> entry : diedMost.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
			if(entry.getValue() > mostKills) {
				found = entry.getKey();
				mostKills = entry.getValue();
			}
		}
		
		if(found == null) {
			System.out.println("You did not die that match!");
		}
		
		return found + " killed you the most by killing you " + mostKills + " times!";
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}
	
	public void setScoreboard(Scoreboard board) {
		this.scoreboard = board;
	}
}
