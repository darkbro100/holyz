package me.mario.ffa;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;

import me.mario.ffa.util.DBUtil;
import me.mario.ffa.util.LocUtil;

public class Commands implements CommandExecutor {

	public boolean onCommand(final CommandSender sender, Command cmd, String label,
			final String[] args) {
		if (!(sender instanceof Player))
			return true;

		if (cmd.getName().equalsIgnoreCase("createmap")) {
			if(!(sender.isOp())) return true;
			if (args.length == 0) {
				return true;
			}

			Player player = (Player) sender;

			Selection sel = Main.we.getSelection(player);

			if (sel == null)
				return true;

			Map map = new Map(args[0], new CuboidSelection(sel.getWorld(),
					sel.getMinimumPoint(), sel.getMaximumPoint()),
					player.getLocation());

			player.sendMessage("map made");

			try {
				Main.fh.saveMap(map);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (cmd.getName().equalsIgnoreCase("topwins")) {
			Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
				public void run() {
					ArrayList<String> list = DBUtil.getTop10("wins");
					for (int i = 0; (i < list.size()) && (i < 10); i++) {
						sender.sendMessage(ChatColor.GRAY + Integer.toString(i + 1)
								+ ". " + (String) list.get(i));
					}
					sender.sendMessage(ChatColor.GREEN + "\nMost Wins");
				}
			});
		} else if (cmd.getName().equalsIgnoreCase("topkills")) {
			Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
				public void run() {
					ArrayList<String> list = DBUtil.getTop10("kills");
					for (int i = 1; (i < list.size()) && (i < 10); i++) {
						sender.sendMessage(ChatColor.GRAY + Integer.toString(i + 1)
								+ ". " + (String) list.get(i));
					}
					sender.sendMessage(ChatColor.GREEN + "\nMost Kills");
				}
			});
		} else if (cmd.getName().equalsIgnoreCase("topelo")) {
			Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
				public void run() {
					ArrayList<String> list = DBUtil.getTop10("elo");
					for (int i = 0; (i < list.size()) && (i < 10); i++) {
						sender.sendMessage(ChatColor.GRAY + Integer.toString(i + 1)
								+ ". " + (String) list.get(i));
					}
					sender.sendMessage(ChatColor.GREEN + "\nMost ELO");
				}
			});
		} else if(cmd.getName().equalsIgnoreCase("spectate")) {
			if(sender instanceof Player) {
				GamePlayer gplayer = GamePlayer.get((Player)sender);
				if(gplayer.isInGame) return true;
				if(!Main.game.isStarted()) return true;
				
				if(gplayer.isSpectating()) {
					gplayer.setSpectating(false);
				} else {
					gplayer.setSpectating(true);
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("tp")) {
			if(sender instanceof Player && args.length > 0) {
				GamePlayer player = GamePlayer.get((Player)sender);
				if(player.isAdmin) {
					Player target = Bukkit.getPlayer(args[0]);
					
					if(target != null) {
						((Player)sender).teleport(target.getLocation());
						sender.sendMessage("\247aTeleported to: " + target.getName());
					}
					
					return true;
				}
				if(!player.isSpectating()) return true;
				
				Player target = Bukkit.getPlayer(args[0]);
				
				if(target != null) {
					((Player)sender).teleport(target.getLocation());
					sender.sendMessage("\247aTeleported to: " + target.getName());
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("tppos") && sender.isOp()) {
			if(args.length == 0) {
				sender.sendMessage("\247c/tppos world,x,y,z");
				return true;
			}
			Location toTpTo = LocUtil.locFromString(args[0]);
			((Player)sender).teleport(toTpTo);
			sender.sendMessage("\247aTPd to: " + args[0]);
		} else if(cmd.getName().equalsIgnoreCase("class")) {

			GamePlayer player = GamePlayer.get((Player)sender);
			if(player.isInGame) return true;
			
			player.modifyingClass = true;
			player.getPlayer().openInventory(player.getGameClass().getInventory());
		} else if(cmd.getName().equalsIgnoreCase("leave")) {
			GamePlayer player = GamePlayer.get((Player)sender);
			if(player.isInGame()) {
				player.getPlayer().sendMessage("You have left the game!");
				Main.game.removePlayer(player);
				player.clear();
				player.teleport();
			} else {
				player.getPlayer().sendMessage("You're not in a game");
			}
		} else if(cmd.getName().equalsIgnoreCase("setadmin")) {
			if(args.length == 0) {
				sender.sendMessage("/setadmin <name>");
				return true;
			}
			
			GamePlayer gp = GamePlayer.get((Player)sender);
			
			GamePlayer target = GamePlayer.get(Bukkit.getPlayer(args[0]));
			
			if(target != null) {
				if(gp.isAdmin) {
					if(target.isAdmin) {
						target.isAdmin = false;
						gp.getPlayer().sendMessage("Removed " + target.getPlayer().getName() + "'s \247cADMIN");
					} else {
						target.isAdmin = true;
						gp.getPlayer().sendMessage("Gave " + target.getPlayer().getName() + " \247cADMIN");
					}
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("gamemode")) {
			GamePlayer gp = GamePlayer.get((Player)sender);
			
			if(gp.isAdmin) {
				if(gp.getPlayer().getGameMode() == GameMode.SURVIVAL) {
					gp.getPlayer().setGameMode(GameMode.CREATIVE);
					gp.getPlayer().sendMessage("Set gamemode to: " + gp.getPlayer().getGameMode());
				} else if(gp.getPlayer().getGameMode() == GameMode.CREATIVE) {
					gp.getPlayer().setGameMode(GameMode.SURVIVAL);
					gp.getPlayer().sendMessage("Set gamemode to: " + gp.getPlayer().getGameMode());
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("vanish")) {
			GamePlayer gp = GamePlayer.get((Player)sender);
			
			if(gp.isAdmin) {
				if(gp.vanished) {
					gp.getPlayer().sendMessage("\247bUNPOOFED...?");
					gp.vanished = false;
					
					for(Player player : Bukkit.getOnlinePlayers()) {
						if(player == gp.getPlayer()) continue;
						
						player.showPlayer(gp.getPlayer());
					}
				} else {
					gp.getPlayer().sendMessage("\247bPOOF!");
					gp.vanished = true;
					for(Player player : Bukkit.getOnlinePlayers()) {
						if(player == gp.getPlayer()) continue;
						
						player.hidePlayer(gp.getPlayer());
					}
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("tphere")) {
			if(sender instanceof Player && args.length > 0) {
				GamePlayer player = GamePlayer.get((Player)sender);
				if(player.isAdmin) {
					Player target = Bukkit.getPlayer(args[0]);
					
					if(target != null) {
						target.teleport(player.getPlayer().getLocation());
						sender.sendMessage("\247aTeleported to you: " + target.getName());
					}
					
					return true;
				}
			}
		} else if(cmd.getName().equalsIgnoreCase("list")) {
			List<String> players = new ArrayList<String>();
			
			for(GamePlayer gp : GamePlayer.players) {
				String name = (gp.isAdmin ? "\247c[ADMIN]\247r " : "") + gp.getPlayer().getName();
				
				players.add(name);
			}
			
			sender.sendMessage("Online Players: (" + Bukkit.getOnlinePlayers().size()  + "/" + Bukkit.getMaxPlayers() + ")");
			sender.sendMessage(players.toString());
		} else if(cmd.getName().equalsIgnoreCase("stopgame")) {
			GamePlayer player = GamePlayer.get((Player)sender);
			if(!player.isAdmin) return true;
			
			if(Main.game.stopped) {
				Main.game.stopped = false;
			} else {
				Main.game.stopped = true;
			}
		} else if(cmd.getName().equalsIgnoreCase("setsign")) {
			if(args.length < 3) {
				sender.sendMessage("could not make sign");
				return true;
			}
			
			SignType type = SignType.valueOf(args[0]);
			String name = args[2];
			int number = Integer.valueOf(args[1]);
			
			Player player = (Player) sender;
			Block block = player.getTargetBlock((Set<Material>) null, 50);
			
			if(block.getState() instanceof Sign) {
				new GameSign(block.getLocation(), type, number - 1, name);
				player.sendMessage("made sign");
			} else {
				player.sendMessage("could not make sign");
			}
		} else if(cmd.getName().equalsIgnoreCase("flag")) {
			if(args.length == 0) {
				sender.sendMessage("/flag <name>");
				return true;
			}
			
			if(GamePlayer.get((Player)sender).isAdmin) {
				Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					sender.sendMessage("Could not find player");
					return true;
				}
				
				GamePlayer gp = GamePlayer.get(target);
				sender.sendMessage("\247d" + gp.getPlayer().getName() + " has " + gp.flags + " flags");
			}
		}
 		return true;
	}
}
