package me.mario.ffa;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GameClass {

	private ItemStack[] contents;

	public static ItemStack[] availableItems = new ItemStack[] {
			new ItemStack(Material.DIAMOND_AXE),
			new ItemStack(Material.DIAMOND_PICKAXE),
			new ItemStack(Material.DIAMOND_HOE),
			new ItemStack(Material.DIAMOND_SPADE),
			new ItemStack(Material.INK_SACK, 64, (byte)11),
			new ItemStack(Material.PAPER),
			new ItemStack(Material.DIAMOND_SWORD),
			new ItemStack(Material.WEB, 64),
			new ItemStack(Material.SHEARS),
			new ItemStack(Material.COMPASS),
			new ItemStack(Material.GOLD_PICKAXE),
			new ItemStack(Material.GOLD_HOE),
			new ItemStack(Material.GOLD_SPADE),
			new ItemStack(Material.IRON_AXE),
			new ItemStack(Material.IRON_PICKAXE),
			new ItemStack(Material.IRON_HOE),
			new ItemStack(Material.IRON_SPADE),
			new ItemStack(Material.STONE_AXE),
			new ItemStack(Material.STONE_PICKAXE),
			new ItemStack(Material.STONE_HOE),
			new ItemStack(Material.STONE_SPADE),
			new ItemStack(Material.WOOD_AXE),
			new ItemStack(Material.WOOD_PICKAXE),
			new ItemStack(Material.WOOD_HOE),
			new ItemStack(Material.WOOD_SPADE)};
	
	public GameClass(ItemStack[] setup) {
		this.contents = setup;
	}

	public ItemStack[] getContents() {
		return contents;
	}

	public void updateContents(ItemStack[] contents) {
		this.contents = contents;
	}

	public static GameClass defaultClass() {
		ItemStack[] array = new ItemStack[] {
				new ItemStack(Material.DIAMOND_AXE),
				new ItemStack(Material.DIAMOND_PICKAXE),
				new ItemStack(Material.DIAMOND_HOE),
				new ItemStack(Material.INK_SACK, 64, (byte)11),
				new ItemStack(Material.PAPER),
				new ItemStack(Material.DIAMOND_SWORD),
				new ItemStack(Material.SHEARS),
				new ItemStack(Material.WEB, 64),
				new ItemStack(Material.COMPASS),};
		
		return new GameClass(array);
	}

	public Inventory getInventory() {
		Inventory inv = Bukkit.createInventory(null, 9, "\2476Class Setup");
		inv.setContents(contents);
		
		return inv;
	}
	
	public static boolean validClassItem(ItemStack stack) {
		for(ItemStack i : availableItems) {
			if(i.isSimilar(stack)) {
				return true;
			}
		}
		
		return false;
	}
}
