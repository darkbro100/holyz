package me.mario.ffa;

import java.util.ArrayList;

import org.bukkit.ChatColor;

public class Team {

	private ArrayList<GamePlayer> members;
	
	public String name;
	public ChatColor color;
	public int kills = 0;

	public Team(String name, ChatColor color) {
		this.members = new ArrayList<GamePlayer>();
		
		this.name = name;
		this.color = color;
	}
	
	public void addMember(GamePlayer player) {
		members.add(player);
		
		System.out.println(player.getPlayer().getName() + " added to " + name);
	}
	
	public void removeMember(GamePlayer player) {
		members.remove(player);
	}
	
	public ArrayList<GamePlayer> getMembers() {
		return members;
	}

	public void addKill() {
		kills ++;
	}
	
	public void resetKills() {
		kills = 0;
	}
}
