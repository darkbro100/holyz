package me.mario.ffa;

import org.bukkit.scheduler.BukkitRunnable;

public class AntiMacroTask extends BukkitRunnable {

	@Override
	public void run() {
		for(GamePlayer player : GamePlayer.players) {
			if(player.clickspersecond >= 10) {
				alertStaff(player);
			}
			player.clickspersecond = 0;
		}
	}
	
	public void alertStaff(GamePlayer player) {
		player.flags++;
		for(GamePlayer staff : GamePlayer.players) {
			if(staff.isAdmin) {
				staff.getPlayer().sendMessage("\247d" + player.getPlayer().getName() + " has " + player.clickspersecond + " clicks per second. They might be using macros!");
			}
		}
	}
}
