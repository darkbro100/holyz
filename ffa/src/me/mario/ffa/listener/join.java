package me.mario.ffa.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import com.brawl.base.BrawlPlayer;

import me.mario.ffa.GamePlayer;
import me.mario.ffa.Main;
import me.mario.ffa.Map;

public class join implements Listener {

	@EventHandler
	public void onPlayerQuit(PlayerJoinEvent event) {
		Main.db.getConnection();
		GamePlayer joined = new GamePlayer(event.getPlayer());
		Main.db.closeConnection();

		Player player = event.getPlayer();

		for (GamePlayer gp : GamePlayer.players) {
			if (gp.vanished) {
				player.hidePlayer(gp.getPlayer());
			} else {
				player.showPlayer(gp.getPlayer());
			}
		}

		player.teleport(Map.getRandomMap().getLobbyLoc());
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (GamePlayer.get(event.getPlayer()) != null) {
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "You are already logged on!");
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		GamePlayer player = GamePlayer.get(event.getPlayer());
		if (player.isAdmin) {
			return;
		}
		if (!player.isInGame) {
			return;
		}
		if (BrawlPlayer.of(event.getPlayer()).getRank().isStaffRank()) {
			return;
		}

		if (event.getMessage().startsWith("/class") || event.getMessage().startsWith("/leave")
				|| event.getMessage().startsWith("/ping"))
			return;

		event.setCancelled(true);
	}
}
