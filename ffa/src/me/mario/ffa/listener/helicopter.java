package me.mario.ffa.listener;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import me.mario.ffa.GamePlayer;
import me.mario.ffa.Main;

public class helicopter extends BukkitRunnable implements Listener {
	
	@Override
	public void run() {
		for(GamePlayer own : GamePlayer.players) {
			if(own.hasheli) {
				System.out.println("Attempting to rework projectile for ghast");
				Entity target = null;
				double distance = 999;
				
				Ghast gh = (Ghast) own.helicopter;
				Block targetb = gh.getTargetBlock((Set<Material>) null, 100);
				
				String[] ar = gh.getCustomName().split("'");
				String name = ar[0];
				GamePlayer player = GamePlayer.get(Bukkit.getPlayer(name));
				
				for(Entity ent : gh.getNearbyEntities(150D, 150D, 150D)) {
					if(ent instanceof Player && ent.getLocation().distance(targetb.getLocation()) < distance) {
						GamePlayer gp = GamePlayer.get((Player)ent);
						
						if(gp.getTeam() != null && player.getTeam() != null) {
							if(gp.getTeam().name.equalsIgnoreCase(player.getTeam().name)) continue;
						}
						
						if(gp.getPlayer().getName().equalsIgnoreCase(player.getPlayer().getName())) continue;
						
						target = ent;
						distance = ent.getLocation().distance(targetb.getLocation());
					}
				}
				if(target == null )continue;
				
				Location loc = gh.getLocation().clone().subtract(0, 5, 0);
				
				double x = target.getLocation().getX() - loc.getX();
				double y = target.getLocation().getY() - loc.getY();
				double z = target.getLocation().getZ() - loc.getZ();
				
				loc.setDirection(new Vector(x, y, z));
				
				Fireball fb = (Fireball) target.getWorld().spawn(loc.subtract(0, 5, 0), Fireball.class);
				
				fb.setMetadata("fireball", new FixedMetadataValue(Main.getInstance(), true));
				fb.setMetadata("name", new FixedMetadataValue(Main.getInstance(), player.getPlayer().getName()));
				
				fb.setVelocity(loc.getDirection().multiply(8));
				fb.setIsIncendiary(false);
				fb.setYield(0);
			}
		}
	}
	
	@EventHandler
	public void onProjLaunch(ProjectileLaunchEvent event) {
		if(event.getEntity().getShooter() instanceof Ghast) {
			System.out.println("Attempting to rework projectile for ghast");
			Entity target = null;
			double distance = 999;
			
			Ghast gh = (Ghast) event.getEntity().getShooter();
			Block targetb = gh.getTargetBlock((Set<Material>) null, 100);
			
			String[] ar = gh.getCustomName().split("'");
			String name = ar[0];
			GamePlayer player = GamePlayer.get(Bukkit.getPlayer(name));
			
			for(Entity ent : gh.getNearbyEntities(150D, 150D, 150D)) {
				if(ent instanceof Player && ent.getLocation().distance(targetb.getLocation()) < distance) {
					GamePlayer gp = GamePlayer.get((Player)ent);
					
					if(gp.getTeam() != null && player.getTeam() != null) {
						if(gp.getTeam().name.equalsIgnoreCase(player.getTeam().name)) continue;
					}
					
					if(gp.getPlayer().getName().equalsIgnoreCase(player.getPlayer().getName())) continue;
					
					target = ent;
					distance = ent.getLocation().distance(targetb.getLocation());
				}
			}
			
			if(target == null) {
				event.getEntity().remove();
				return;
			}
			
			Projectile pl = event.getEntity();
			Location loc = pl.getLocation();
			
			double x = target.getLocation().getX() - loc.getX();
			double y = target.getLocation().getY() - loc.getY();
			double z = target.getLocation().getZ() - loc.getZ();
			
			pl.remove();
			loc.setDirection(new Vector(x, y, z));
			
			Fireball fb = (Fireball) target.getWorld().spawn(loc, Fireball.class);
			
			fb.setMetadata("fireball", new FixedMetadataValue(Main.getInstance(), true));
			fb.setMetadata("name", new FixedMetadataValue(Main.getInstance(), player.getPlayer().getName()));
			
			fb.setVelocity(loc.getDirection().multiply(8));
			fb.setIsIncendiary(false);
			fb.setYield(0);
		}
		
	}
}
