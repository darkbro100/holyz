package me.mario.ffa.listener;

import java.util.ArrayList;
import java.util.List;

import me.mario.ffa.GameClass;
import me.mario.ffa.GamePlayer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class inventory implements Listener {

	@EventHandler
	public void onInvClose(InventoryCloseEvent event) {
		if(event.getPlayer() instanceof Player) {
			GamePlayer player = GamePlayer.get((Player)event.getPlayer());
			player.modifyingClass = false;
			player.selectingItem = false;
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent event) {
		if(event.getSlotType() == SlotType.OUTSIDE || event.getSlotType() == SlotType.ARMOR) return;
		if(event.getWhoClicked() instanceof Player) {
			GamePlayer player = GamePlayer.get((Player)event.getWhoClicked());
			if(player.modifyingClass) {
				player.slot = event.getSlot();
				player.selectingItem = true;
				if(player.slot > 8) {
					event.setCancelled(true);
					player.selectingItem = false;
					return;
				}
				player.getPlayer().closeInventory();
				Inventory inv = Bukkit.createInventory(null, 27);
				inv.setContents(GameClass.availableItems);
				player.getPlayer().openInventory(inv);
				player.modifyingClass = false;
				player.selectingItem = true;
				event.setCancelled(true);
				player.slot = event.getSlot();
			} else if(player.selectingItem) {
				ItemStack item = event.getCurrentItem();
				System.out.println(String.format("%s %s", player.slot, item.getType().name()));
				ItemStack[] items = player.getGameClass().getContents();
				List<ItemStack> litems = new ArrayList<ItemStack>();
				for(ItemStack i : items) {
					litems.add(i);
				}
				
				litems.set(player.slot, item);
				player.gclass.updateContents(litems.toArray(new ItemStack[litems.size()]));
				player.getPlayer().closeInventory();
				player.getPlayer().openInventory(player.getGameClass().getInventory());
				player.modifyingClass = true;
				player.selectingItem = false;
				event.setCancelled(true);
			}
		}
	}
	
}
