package me.mario.ffa.listener;

import me.mario.ffa.GamePlayer;
import me.mario.ffa.Main;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class quit implements Listener {

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		GamePlayer player = GamePlayer.get(event.getPlayer());
		
		if(player.isInGame) {
			Main.game.removePlayer(player);
			player.clear();
			player.teleport();
		}
		
		GamePlayer.remove(player);
	}
}
