package me.mario.ffa.listener;

import java.util.ArrayList;

import me.mario.ffa.GamePlayer;
import me.mario.ffa.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("deprecation")
public class extra implements Listener {

	public static ArrayList<Location> states = new ArrayList<Location>();
	private ArrayList<String> delay = new ArrayList<String>();
	
	@EventHandler
	public void onPlayerpickup(PlayerDropItemEvent event) {
		if(!Main.game.isStarted()) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if(GamePlayer.get(event.getPlayer()).isAdmin) return;
		
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if((event.getBlock().getType() == Material.WEB)) {
			states.add(event.getBlock().getLocation());
			return;
		} else {
			if(GamePlayer.get(event.getPlayer()).isAdmin) return;
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			GamePlayer.get(event.getPlayer()).clickspersecond++;
		}
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getClickedBlock() != null) {
				if(event.getClickedBlock().getType() == Material.CHEST) {
					if(!event.getClickedBlock().hasMetadata("loc")) {
						event.setCancelled(true);
					}
				}
			}
		}
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getClickedBlock() != null) {
				if(event.getClickedBlock().getType() == Material.WEB && event.getPlayer().getItemInHand().getType() == Material.SHEARS) {
					event.getClickedBlock().setType(Material.AIR);
					for(Player player : Bukkit.getOnlinePlayers()) {
						player.playSound(event.getPlayer().getLocation(), Sound.ITEM_BREAK, 3F, 4F);
					}
				}
			}
		}
		
			if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				ItemStack item = event.getPlayer().getItemInHand();
				
				if(item.getType() == Material.INK_SACK && item.getData().getData() == (byte)11) {
					if(delay .contains(event.getPlayer().getName())) return;
					
					if(event.getPlayer().getHealth() + 7.5 > event.getPlayer().getMaxHealth()) {
						event.getPlayer().setHealth(event.getPlayer().getMaxHealth());
					} else {
						event.getPlayer().setHealth(event.getPlayer().getHealth() + 7.5D);
					}
					
					event.getPlayer().setFoodLevel(event.getPlayer().getFoodLevel() + 8);
					
					final String name = event.getPlayer().getName();
					event.getPlayer().getItemInHand().setAmount(event.getPlayer().getItemInHand().getAmount() - 1);
					
					Player p = event.getPlayer();
					
					if (p.getItemInHand().getAmount() <= 1)
						p.getInventory().removeItem(p.getItemInHand());
					delay.add(name);
					Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
						public void run() {
							delay.remove(name);
						}
					}, 20L * 1L);
				}
			}
	}
	
//	@EventHandler(priority = EventPriority.MONITOR)
//	public void onPlayerChat(AsyncPlayerChatEvent event) {
//		GamePlayer player = GamePlayer.get(event.getPlayer());
//		
//		if(player == null) return;
//		
//		event.setFormat((player.isAdmin ? " [\247cADMIN\247r] " : "") + "[\247a" + player.elo + "\247r] " + event.getFormat());
//		
//		if(Main.game.isStarted()) {
//			if(player.getTeam() == null) return;
//			
//			for(GamePlayer member : player.getTeam().getMembers()) {
//				member.getPlayer().sendMessage("[" + player.getTeam().color + player.getTeam().name + "\247r] " + "[\247a" + player.elo + "\247r] " + player.getTeam().color + event.getPlayer().getName() + "\2477: \247r" + event.getMessage());
//				event.setCancelled(true);
//			}
//		}
//	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if(event.getCreatureType() == CreatureType.GHAST) return;
		if(event.getCreatureType() == CreatureType.WOLF) return;
		
		event.setCancelled(true);
	}
	
}
