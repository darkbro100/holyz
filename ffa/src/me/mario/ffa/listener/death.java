package me.mario.ffa.listener;



import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.brawl.base.util.scheduler.Sync;
import com.orange451.pvpgunplus.PVPGunPlus;
import com.orange451.pvpgunplus.events.PVPGunPlusGunDamageEntityEvent;

import me.mario.ffa.Game.GameType;
import me.mario.ffa.GamePlayer;
import me.mario.ffa.Main;
import me.mario.ffa.Map;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand;

public class death implements Listener {

	@EventHandler
	public void onPlayerDeath(final PlayerDeathEvent event) {
		final GamePlayer player = GamePlayer.get(event.getEntity());
		
		player.resetKillstreak();
		PVPGunPlus.getPlugin().getGunPlayer(event.getEntity()).reloadAllGuns();
		
		if(player.getTeam() != null && player.getTeam().getMembers().size() > 1) {
			for(GamePlayer member : player.getTeam().getMembers()) {
				if(member == player) continue;
				member.getPlayer().sendMessage("\247a" + player.getPlayer().getName() + "\2477 was killed by " + (player.getPlayer().getKiller() != null ? player.getPlayer().getKiller().getName() : "null"));
			}
		}
		
		Player killer = event.getEntity().getKiller();
		if(killer != null) {
			GamePlayer pkiller = GamePlayer.get(killer);
			pkiller.addKill(player);
			
			if(pkiller.getTeam() != null && pkiller.getTeam().getMembers().size() > 1) {
				for(GamePlayer member : pkiller.getTeam().getMembers()) {
					if(member == pkiller) continue;
					member.getPlayer().sendMessage("\247a" + pkiller.getPlayer().getName() + "\2477 killed " + player.getPlayer().getName());
				}
			}
			
			killer.sendMessage("\2477Killed \247a" + player.getPlayer().getName());
		} else {
			EntityDamageEvent dmg = event.getEntity().getLastDamageCause();
			
			if(dmg instanceof EntityDamageByEntityEvent) {
				EntityDamageByEntityEvent lastDamage = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();
				
				if(lastDamage != null) {
					if(lastDamage.getDamager() instanceof Wolf) {
						LivingEntity ent = (LivingEntity)lastDamage.getDamager();
						
						String[] ar = ent.getCustomName().split("'");
						String name = ar[0];
						
						GamePlayer dmger = GamePlayer.get(Bukkit.getPlayer(name));
						dmger.addKill(player);
						
						if(Main.game.getGameType() != GameType.FFA) {
							for(GamePlayer member : dmger.getTeam().getMembers()) {
								if(member == dmger) continue;
								member.getPlayer().sendMessage("\247a" + dmger.getPlayer().getName() + "\2477 killed " + player.getPlayer().getName());
							}
						}
						
						dmger.getPlayer().sendMessage("\2477Killed \247a" + player.getPlayer().getName());
					}
					
					if(lastDamage.getDamager().getType() == EntityType.FIREBALL) {
						if(lastDamage.getDamager().hasMetadata("name")) {
							GamePlayer dmger = GamePlayer.get(Bukkit.getPlayer(lastDamage.getDamager().getMetadata("name").get(0).asString()));
							
							dmger.addKill(player);
							
							if(Main.game.getGameType() != GameType.FFA) {
								for(GamePlayer member : dmger.getTeam().getMembers()) {
									if(member == dmger) continue;
									member.getPlayer().sendMessage("\247a" + dmger.getPlayer().getName() + "\2477 killed " + player.getPlayer().getName());
								}
							}
							
							dmger.getPlayer().sendMessage("\2477Killed \247a" + player.getPlayer().getName());
						}
					}
				}
			}
		}
		
		player.getPlayer().sendMessage("\2477You were killed by \247a" + (killer != null ? killer.getName() : "null"));
		
		if(player.isInGame) {
			event.setDeathMessage(null);
			player.addDeath();
		}
		
		event.getDrops().clear();
		
		if(Main.game.getGameType() == GameType.SND) {
			Main.game.removePlayer(player);
		}
		
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			public void run() {
				((CraftPlayer) event.getEntity()).getHandle().playerConnection.a(new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN));
			}
		}, 1L);
	}
	
	@EventHandler
	public void onRespawn(final PlayerRespawnEvent event) {
		if(Main.game.getGameType() != GameType.SND) {	
			if(Main.game.isStarted()) {
				final GamePlayer player = GamePlayer.get(event.getPlayer());
				player.resetKillstreak();
				
				Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
					public void run() {
						if(player.isInGame()) {
							player.giveKit();
							player.setGodMode(true);
							player.getPlayer().setHealth(20D);
						}
					}
				}, 10L);
				
				Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
					public void run() {
						if(player.isInGame()) {
							event.getPlayer().teleport((Main.game.getCurrentMap().getRandomLocation(
									event.getPlayer(),
									(int) Main.game.getCurrentMap().getSelection()
											.getMinimumPoint().getX(),
									(int) Main.game.getCurrentMap().getSelection()
											.getMaximumPoint().getX(),
									(int) Main.game.getCurrentMap().getSelection()
											.getMinimumPoint().getZ(),
									(int) Main.game.getCurrentMap().getSelection()
											.getMaximumPoint().getZ(),
									false)));
						}
					}
				}, 20L * 3L);
				
				Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
					public void run() {
						player.setGodMode(false);
					}
				}, 20L * 5L);
			}
		} else {
			event.setRespawnLocation(Map.getRandomMap().getLobbyLoc());
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamageBy(EntityDamageByEntityEvent event) {
		
		if(event.getEntity() instanceof Player) {
			GamePlayer player = GamePlayer.get((Player)event.getEntity());
			
			if(player.godmoded) {
				event.setCancelled(true);
				return;
			}
			
			if(!player.isInGame()) {
				event.setCancelled(true);
				return;
			}
			
			if(event.getDamager() instanceof Projectile) {
				Projectile p = (Projectile) event.getDamager();
				
				if(p.getType() == EntityType.FIREBALL) {
					if(p.hasMetadata("fireball")) {
						if(p.getMetadata("fireball").get(0).asBoolean()) {
							event.setDamage(event.getDamage() * 20);
						}
					}
				}
				
				if(p.getShooter() instanceof Player) {
					GamePlayer shooter = GamePlayer.get((Player)p.getShooter());
					
					if(Main.game.getGameType() == GameType.FFA) return;
					
					if(shooter == null || player == null) return;
					
					if(shooter.getTeam().equals(player.getTeam())) {
						shooter.getPlayer().sendMessage("\2477You cannot attack friends!");
						event.setCancelled(true);
					}
				}
			} else if(event.getDamager() instanceof Player) {
				GamePlayer damager = GamePlayer.get((Player)event.getDamager());
				
				if(damager.godmoded) {
					event.setCancelled(true);
					return;
				}
				
				if(!damager.isInGame) {
					event.setCancelled(true);
					return;
				}
				
				if(Main.game.getGameType() == GameType.FFA) return;
				
				if(damager.getTeam().equals(player.getTeam())) {
					damager.getPlayer().sendMessage("\2477You cannot attack friends!");
					event.setCancelled(true);
				}
			} else if(event.getDamager() instanceof Wolf) {
				LivingEntity ent = (LivingEntity)event.getDamager();
				
				String[] ar = ent.getCustomName().split("'");
				String name = ar[0];
				
				GamePlayer dmger = GamePlayer.get(Bukkit.getPlayer(name));
				
				if(Main.game.getGameType() != GameType.FFA) {
					if(dmger.getTeam().name.equalsIgnoreCase(player.getTeam().name)) {
						event.setCancelled(true);
					} else {
						event.setDamage(event.getDamage() * 8.5D);
					}
				} else {
					if(dmger.getPlayer().getName().equalsIgnoreCase(player.getPlayer().getName())) {
						event.setCancelled(true);
					} else {
						event.setDamage(event.getDamage() * 8.5D);
					}
				}
			}
		} else if(event.getEntity() instanceof Wolf || event.getEntity() instanceof Ghast) {
			LivingEntity ent = (LivingEntity)event.getEntity();
			
			String[] ar = ent.getCustomName().split("'");
			String name = ar[0];
			
			GamePlayer player = GamePlayer.get(Bukkit.getPlayer(name));
			
			if(event.getDamager() instanceof Projectile) {
				Projectile p = (Projectile) event.getDamager();
				
				if(p.getShooter() instanceof Player) {
					GamePlayer shooter = GamePlayer.get((Player)p.getShooter());
					
					if(Main.game.getGameType() == GameType.FFA) {
						if(shooter.getPlayer().getName().equalsIgnoreCase(player.getPlayer().getName())) {
							event.setCancelled(true);
						}
						return;
					}
					
					if(shooter.getTeam().equals(player.getTeam())) {
						event.setCancelled(true);
						shooter.getPlayer().sendMessage("\2477You cannot attack friends!");
					}
				}
			} else if(event.getDamager() instanceof Player) {
				GamePlayer damager = GamePlayer.get((Player)event.getDamager());
				
				if(damager.godmoded) {
					event.setCancelled(true);
					return;
				}
				
				if(!damager.isInGame) {
					event.setCancelled(true);
					return;
				}
				
				if(Main.game.getGameType() == GameType.FFA) {
					if(damager.getPlayer().getName().equalsIgnoreCase(player.getPlayer().getName())) {
						event.setCancelled(true);
						damager.getPlayer().sendMessage("\2477You cannot attack friends!");
					}
					
					return;
				}
				
				if(damager.getTeam().equals(player.getTeam())) {
					damager.getPlayer().sendMessage("\2477You cannot attack friends!");
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onGunShoot(PVPGunPlusGunDamageEntityEvent event) {
		if(!(event.getEntityDamaged() instanceof Player))
			return;
		
		Player d = (Player) event.getEntityDamaged();
		Player k = event.getKillerAsPlayer();
		
		GamePlayer killer = GamePlayer.get(k);
		GamePlayer dead = GamePlayer.get(d);
		
		if(killer.getTeam() != null && dead.getTeam() != null && killer.getTeam().name.equalsIgnoreCase(dead.getTeam().name)) {
			event.setCancelled(true);
			k.sendMessage(ChatColor.GRAY + "You cannot attack friends!");
		}
		
		if(!event.isCancelled()) {
			d.getWorld().playEffect(d.getLocation().add(0.0D, 0.8D, 0.0D), Effect.getById(2001),
					Material.BARRIER);
		}
	}
	
	@EventHandler
	public void onWolfAttack(EntityDamageByEntityEvent event) {
		if(!(event.getEntity() instanceof Player)) {
			return;
		}
		
		if(!(event.getDamager() instanceof Wolf)) {
			return;
		}
		
		GamePlayer gp = GamePlayer.get((Player)event.getEntity());
		if(gp.recentAttackers.contains(event.getDamager())) {
			event.setCancelled(true);
			return;
		}
		
		gp.recentAttackers.add(event.getDamager());
		Sync.get().delay(30).run(() -> gp.recentAttackers.remove(event.getDamager()));
		PVPGunPlus.resetPlayerDamage((Player) event.getEntity(), 0);
	}
	
	
}
