package me.mario.ffa;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.brawl.base.BrawlPlayer;
import com.brawl.base.BrawlPlugin;
import com.brawl.base.chat.ChatHandler;
import com.brawl.base.cosm.api.CosmCategory;
import com.brawl.base.cosm.api.CosmRules;
import com.google.common.collect.Sets;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import me.mario.ffa.listener.death;
import me.mario.ffa.listener.extra;
import me.mario.ffa.listener.helicopter;
import me.mario.ffa.listener.inventory;
import me.mario.ffa.listener.join;
import me.mario.ffa.listener.quit;
import me.mario.ffa.util.DBUtil;
import me.mario.ffa.util.FileHandler;
import me.signatured.shared.chat.C;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class Main extends JavaPlugin {

	public static Game game;
	private static Main instance;
	public static FileHandler fh;
	public static WorldEditPlugin we;
	public static DBUtil db;
	
	public void onEnable() {
		instance = this;
		fh = new FileHandler(this);
		game = new Game();
		db = new DBUtil(fh.getIp(), fh.getPort(), fh.getUser(), fh.getPassword(), fh.getDB());
		we = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
		
		new GameTask().runTaskTimer(this, 20L, 20L);
		new DogTask().runTaskTimer(this, 20L, 20L);
		new AntiMacroTask().runTaskTimer(this, 20L, 20L);
		new HelicopterTask().runTaskTimer(this, 20L, 20L);
		new SignUpdate().runTaskTimerAsynchronously(this, 20L * 60L, 20L * 60L);
		new helicopter().runTaskTimer(this, 40L, 40L);
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			public void run() {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop RESTARTING!");
			}
		}, 20L * 10800L);
		
		Bukkit.getServer().getPluginManager().registerEvents(new join(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new quit(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new death(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new inventory(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new extra(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new helicopter(), this);
		
		getCommand("createmap").setExecutor(new Commands());
		getCommand("topwins").setExecutor(new Commands());
		getCommand("topkills").setExecutor(new Commands());
		getCommand("topelo").setExecutor(new Commands());
		getCommand("spectate").setExecutor(new Commands());
		getCommand("tp").setExecutor(new Commands());
		getCommand("class").setExecutor(new Commands());
		getCommand("leave").setExecutor(new Commands());
		getCommand("setadmin").setExecutor(new Commands());
		getCommand("tphere").setExecutor(new Commands());
		getCommand("vanish").setExecutor(new Commands());
		getCommand("gamemode").setExecutor(new Commands());
		getCommand("list").setExecutor(new Commands());
		getCommand("stopgame").setExecutor(new Commands());
		getCommand("altcheck").setExecutor(new Commands());
		getCommand("setsign").setExecutor(new Commands());
		getCommand("flag").setExecutor(new Commands());
		getCommand("tppos").setExecutor(new Commands());
		for(Player player : Bukkit.getOnlinePlayers()) {
			new GamePlayer(player);
		}
		fh.loadMaps();
		fh.loadSigns();
		
		Set<CosmCategory> allowed = Sets.newHashSet(CosmCategory.PARTICLES, CosmCategory.TAGS);
		CosmRules.add((c, p) -> {
			return allowed.contains(c.getCategory());
		});
		CosmRules.disable(CosmCategory.values());
		
		BrawlPlugin.getInstance().registerChatHandler(new ChatHandler() {
			@Override
			public List<BrawlPlayer> handleChatMessage(BrawlPlayer sender, Function<BrawlPlayer, String> messageFunc,
					List<BrawlPlayer> receivers) {
				GamePlayer gp = GamePlayer.get(sender.handle());
				
				String format = "[" + C.GREEN + gp.elo + C.R + "] " + sender.getChatPrefix() + sender.getName() + ChatColor.DARK_GRAY + " » " + ChatColor.RESET;
				receivers.forEach(bp -> bp.sendMessage(format + messageFunc.apply(bp)));
				
				return receivers;
			}
		});
		
		new DisplaynameFormatter();
		
		BrawlPlugin.getInstance().setTabNameFormatter(bp -> {
			String poof = "";

			if (bp.getHiddenFrom() != null)
				poof = bp.getHiddenFrom().getColor() + "❖ " + C.R;
			
			GamePlayer gp = GamePlayer.get(bp.handle());
			String prefix = C.GRAY;
			if(gp.getTeam() != null) {
				prefix = gp.getTeam().color.toString();
			}
			
			return poof + prefix + bp.getDisplayName();
		});
		
		new StatsCommand().registerCommandForce();
	}
	
	public void onDisable() {
		db.getConnection();
		for(GamePlayer player : GamePlayer.players) {
			db.save(player);
		}
		db.closeConnection();
		
		for(Map map : Map.getMaps()) {
			try {
				fh.saveMap(map);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		for(GameSign sign : GameSign.signs) {
			fh.saveSign(sign);
		}
		
		GamePlayer.players.clear();
		instance = null;
	}

	public static Plugin getInstance() {
		return instance;
	}
	
	  public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
	  {
	    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
	    
	    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
	    connection.sendPacket(packetPlayOutTimes);
	    if (subtitle != null)
	    {
	      subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
	      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
	      IChatBaseComponent titleSub = ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
	      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, titleSub);
	      connection.sendPacket(packetPlayOutSubTitle);
	    }
	    if (title != null)
	    {
	      title = title.replaceAll("%player%", player.getDisplayName());
	      title = ChatColor.translateAlternateColorCodes('&', title);
	      IChatBaseComponent titleMain = ChatSerializer.a("{\"text\": \"" + title + "\"}");
	      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleMain);
	      connection.sendPacket(packetPlayOutTitle);
	    }
	  }
	  
	  public static void sendActionBar(String message, Player player) {
		  String blockbounty = message;
        CraftPlayer pl = (CraftPlayer)player;
        IChatBaseComponent cbc = ChatSerializer.a("{\"text\":\"" + ChatColor.translateAlternateColorCodes('&', blockbounty) + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
        pl.getHandle().playerConnection.sendPacket(ppoc);
	  }
	  
	  public static void broadcast(String message) {
		  for(Player p : Bukkit.getOnlinePlayers()) {
			  p.sendMessage(message);
		  }
		  Bukkit.getConsoleSender().sendMessage(message);
	  }
	
}
