package me.mario.ffa;

import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

public class HelicopterTask extends BukkitRunnable {

	public void run() {
		if(GamePlayer.players.size() > 0) {
			for(GamePlayer player : GamePlayer.players) {
				if(!player.hasheli) continue;
				
				LivingEntity ghast = player.helicopter;
				
				/* x = player.getPlayer().getLocation().getBlockX();
				int y = player.getPlayer().getLocation().getBlockY();
				int z = player.getPlayer().getLocation().getBlockZ();
			    
				EntityGhast eg = ((CraftGhast) ghast).getHandle();
                
				//PathEntity path = eg.world.a(eg, x, y, z, (float) eg.getAttributeInstance(GenericAttributes.b).getValue(), true, false, false, true);
                //eg.getNavigation().a(path, 1);
                
                eg.setGoalTarget(((CraftPlayer)player.getPlayer()).getHandle());*/
				
				if(ghast.getLocation().distance(player.getPlayer().getLocation()) > 64) {
					ghast.teleport(player.getPlayer().getLocation().add(0, 10, 0));
				}
			}
		}
	}
}
