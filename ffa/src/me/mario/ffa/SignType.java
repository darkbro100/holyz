package me.mario.ffa;

public enum SignType {

	ELO, WINS, KILLS, BEST_KILLSTREAK, PLAYED, GAME_KILLS, TIME_PLAYED, KDR;
	
}
