package me.mario.ffa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

import me.mario.ffa.listener.extra;

public class Game {

	public enum GameType {
		SND, FFA, TDM;
	}

	private boolean started;
	
	private GameType type;
	
	private ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
	private Team red;
	private Team blue;
	private Team orange;
	private Team green;
	private Team yellow;
	
	private Map currentMap;

	private ArrayList<Team> teams = new ArrayList<Team>();

	public boolean stopped = false;

	public Game() {
		started = false;

		this.red = new Team("red", ChatColor.RED);
		this.blue = new Team("blue", ChatColor.BLUE);
		this.orange = new Team("gold", ChatColor.GOLD);
		this.green = new Team("green", ChatColor.GREEN);
		this.yellow = new Team("yellow", ChatColor.YELLOW);
	}

	public void addPlayer(GamePlayer player) {
		players.add(player);

		player.isInGame = true;
	}

	public void removePlayer(GamePlayer player) {
		if (players.contains(player)) {
			System.out.println("removed " + player.getPlayer().getName());
			players.remove(player);
			player.isInGame = false;

			if(player.getTeam() != null) {
				player.getTeam().removeMember(player);
				player.setTeam(null);	
			}
			
			if(player.hasdogs) {
				for(LivingEntity dog : player.dogs) {
					dog.remove();
				}
				player.dogs.clear();
				player.hasdogs = false;
			}
			
			if(player.hasheli) {
				player.setHasHelicopter(false);
			}
			
			if(type != GameType.FFA) {
				for (int i = 0; i < teams.size(); i++) {
					Team team = teams.get(i);
					if (team.getMembers().size() <= 0) {
						removeTeam(team);
					}
				}
			} else {
				if(players.size() <= 1) {
					end();
				}
			}
		}
	}

	private void removeTeam(Team team) {
		Main.broadcast(team.color + team.name
				+ "\247r has been eliminated!");
		teams.remove(team);

		if (teams.size() <= 1) {
			end();
		}
	}

	public ArrayList<GamePlayer> getPlayers() {
		return players;
	}

	public Map getCurrentMap() {
		return currentMap;
	}

	public void start() {
		this.currentMap = Map.getRandomMap();
		this.type = Map.getRandomGameType();
		
		System.out.println("started");

		if (teams.size() <= 0)
			resetTeams();
		Collections.shuffle(GamePlayer.players);

		if(this.type != GameType.FFA) {

			int teamNum = 0;
			
			for (int i = 0; i < GamePlayer.players.size(); i++) {
				final GamePlayer player = GamePlayer.players.get(i);
				
				Team team = teams.get(teamNum);
				team.addMember(player);
				player.setTeam(team);

				if (teamNum < teams.size() - 1) {
					teamNum += 1;
				} else {
					teamNum = 0;
				}
			}
		}

		System.out.println("MAP: " + currentMap.getName());
		System.out.println("TYPE: " + type.name());
		
		for(int i = 0; i < GamePlayer.players.size(); i++) {
			final GamePlayer player = GamePlayer.players.get(i);
			
			addPlayer(player);
			player.giveKit();
			player.isInGame = true;
			player.getPlayer().setGameMode(GameMode.SURVIVAL);
			player.addGamesPlayed();
			Bukkit.getServer().getScheduler()
					.runTaskLater(Main.getInstance(), new Runnable() {
						public void run() {
							if(!player.isInGame()) return;
							player.getPlayer().teleport(
									currentMap.getRandomLocation(
											player.getPlayer(),
											(int) currentMap.getSelection()
													.getMinimumPoint().getX(),
											(int) currentMap.getSelection()
													.getMaximumPoint().getX(),
											(int) currentMap.getSelection()
													.getMinimumPoint().getZ(),
											(int) currentMap.getSelection()
													.getMaximumPoint().getZ(),
											false));
						}
					}, 20L * 10L);
			
		}		
		for (GamePlayer player : players) {
			player.getPlayer().sendMessage(
					"\247aMAP LOADED: \247d" + currentMap.getName());
			
			player.getPlayer().sendMessage("\247aGAME TYPE: \247d" + type.name());
			if(player.getTeam() != null) {
				Main.sendTitle(player.getPlayer(), 20, 80, 20, "\247aMAP/MODE: \247d" + currentMap.getName() + "/" + this.type.name(), "TEAM: " + player.getTeam().color + player.getTeam().name);
				player.getPlayer().sendMessage(
						"You are on team " + player.getTeam().color
								+ player.getTeam().name);
				player.getPlayer().sendMessage("Players on your team:");
				System.out.println("Sent title1 to: " + player.getPlayer().getName());
				for (GamePlayer tplayer : player.getTeam().getMembers()) {
					player.getPlayer().sendMessage(tplayer.getPlayer().getName());
				}
			} else {
				System.out.println("Sent title2 to: " + player.getPlayer().getName());
				Main.sendTitle(player.getPlayer(), 0, 40, 0, "\247aMAP: \247d" + this.type.name(), "\247aMODE: \247d" + this.type.name());
			}
		}

		started = true;
		GameTask.timeUntilMatchEnds = 300;
	}

	public void end() {
		System.out.println("ended");
		started = false;

		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
			public void run() {
				System.out.println("Updating Signs");
				GameSign.update();
			}
		});
		
		if(type == GameType.SND) {
			for (Team team : teams) {
				if (team.getMembers().size() > 0) {
					Main.broadcast(team.color + team.name
							+ " \247rhas won the game");
					System.out.println("size for team is "
							+ team.getMembers().size());
					for (int i = 0; i < team.getMembers().size(); i++) {
						System.out.println(i + "/" + team.getMembers().size());
						final GamePlayer player = team.getMembers().get(i);

						shootfirework(player.getPlayer());

						player.getPlayer().sendMessage("\247aYou win!");

						player.setTeam(null);
//						player.resetGameStats();
						player.clear();
						player.addWin();
						player.isInGame = false;

						Bukkit.getScheduler().runTaskLater(Main.getInstance(),
								new Runnable() {
									public void run() {
										if (player != null) {
											player.teleport();
											player.getPlayer().setHealth(
													player.getPlayer()
															.getMaxHealth());
										}
									}
								}, 20L * 3L);

						System.out.println("removed from " + team.name + " - "
								+ player.getPlayer());
					}

					team.getMembers().clear();
				}
			}
		} else if(type == GameType.FFA){
			GamePlayer winningPlayer = null;
			int winningKills = 0;
			for(GamePlayer player : players) {
				if(player.gamekills >= winningKills) {
					winningKills = player.gamekills;
					winningPlayer = player;
				}
			}

			if(winningPlayer == null) winningPlayer = GamePlayer.players.get(0);
			Main.broadcast("\247d" + winningPlayer.getPlayer().getName() + "\247r has won the game with \247d" + winningKills + "\247r kills!");
			
			shootfirework(winningPlayer.getPlayer());

			winningPlayer.getPlayer().sendMessage("\247aYou win!");

			winningPlayer.setTeam(null);
//			winningPlayer.resetGameStats();
			winningPlayer.clear();
			winningPlayer.addWin();
			winningPlayer.isInGame = false;
		} else if(type == GameType.TDM){
			int killsBefore = 0;
			Team winningTeam = null;
			for(Team team : teams) {
				if(team.kills >= killsBefore) {
					killsBefore = team.kills;
					winningTeam = team;
				}
			}
			
			GamePlayer mostKills = null;
			int gameKils = 0;
			for(GamePlayer tPlayer : winningTeam.getMembers()) {
				if(tPlayer.gamekills > gameKils) {
					gameKils = tPlayer.gamekills;
					mostKills = tPlayer;
				}
			}
			
			GamePlayer mostOverallKills = null;
			int overallKills = 0;
			for(GamePlayer player : players) {
				if(player.gamekills > overallKills) {
					overallKills = player.gamekills;
					mostOverallKills = player;
				}
			}
			if(mostKills == null) mostKills = GamePlayer.players.get(0);
			if(mostOverallKills == null) mostOverallKills = GamePlayer.players.get(0);
			Main.broadcast(winningTeam.color + winningTeam.name + "\247r has won the game with \247d" + killsBefore + "\247r kills!");
			Main.broadcast(winningTeam.color + mostKills.getPlayer().getName() + "\247r got the most kills on their team with \247d" + gameKils + "\247r kills!");
			Main.broadcast("\247d" + mostOverallKills.getPlayer().getName() + "\247r got the most overall kills with \247d" + overallKills + "\247r kills!");
			
			for(final GamePlayer player : winningTeam.getMembers()) {
				shootfirework(player.getPlayer());

				player.getPlayer().sendMessage("\247aYou win!");

				player.setTeam(null);
//				player.resetGameStats();
				player.clear();
				player.addWin();
				player.isInGame = false;
			}
			
			winningTeam.getMembers().clear();
			
			for(Team team : teams) {
				for(GamePlayer player : team.getMembers()) {
					player.setTeam(null);
					player.resetGameStats();
					player.clear();
					player.isInGame = false;
				}
				team.getMembers().clear();
			}
		}

		for (final GamePlayer player : GamePlayer.players) {
			if (player.isSpectating()) {
				player.setSpectating(false);
			}
			
			if(player.hasdogs) {
				for(LivingEntity dog : player.dogs) {
					dog.remove();
				}
				player.dogs.clear();
				player.hasdogs = false;
			}
			
			if(player.hasheli) {
				player.setHasHelicopter(false);
			}
			
			player.getPlayer().sendMessage("\247b" + player.getKilledMost());
			player.getPlayer().sendMessage("\247b" + player.getDiedMost());
			player.setTeam(null);
			player.resetGameStats();
			player.clear();
			player.isInGame = false;
			
			Bukkit.getScheduler().runTaskLater(Main.getInstance(),
					new Runnable() {
						public void run() {
							if (player != null) {
								player.teleport();
								player.getPlayer().setHealth(
										player.getPlayer()
												.getMaxHealth());
							}
						}
					}, 20L * 3L);
		}

		for (Location state : extra.states) {
			if (state.getBlock().getType() == Material.WEB)
				state.getBlock().setType(Material.AIR);

			System.out.println("Set block to air");
		}

		for(Team team : teams) {
			team.resetKills();
		}
		
		extra.states.clear();
		teams.clear();
		players.clear();
		GameTask.timeUntilEnds = 30;
	}

	private void resetTeams() {
		if (Bukkit.getOnlinePlayers().size() % 5 == 0) {
			teams.add(red);
			teams.add(blue);
			teams.add(orange);
			teams.add(green);
			teams.add(yellow);
		} else if (Bukkit.getOnlinePlayers().size() % 4 == 0) {
			teams.add(red);
			teams.add(blue);
			teams.add(orange);
			teams.add(yellow);
		} else if (Bukkit.getOnlinePlayers().size() % 3 == 0) {
			teams.add(red);
			teams.add(blue);
			teams.add(orange);
		} else {
			teams.add(red);
			teams.add(blue);
		}
	}

	public boolean isStarted() {
		return started;
	}

	public ArrayList<Team> getTeams() {
		return teams;
	}

	public static void shootfirework(Player p) {
		Firework fw = (Firework) p.getWorld().spawn(p.getLocation(),
				Firework.class);
		FireworkMeta fm = fw.getFireworkMeta();

		Random r = new Random();

		Type type = null;

		List<Type> types = new ArrayList<Type>();

		for (Type t : Type.values()) {
			types.add(t);
		}

		Collections.shuffle(types);

		type = types.get(0);

		Color c1 = Color
				.fromBGR(r.nextInt(255), r.nextInt(255), r.nextInt(255));
		Color c2 = Color
				.fromBGR(r.nextInt(255), r.nextInt(255), r.nextInt(255));

		FireworkEffect eff = FireworkEffect.builder().flicker(r.nextBoolean())
				.withColor(c1).withFade(c2).with(type).trail(r.nextBoolean())
				.build();

		int power = r.nextInt(2) + 1;
		fm.setPower(power);
		fm.addEffect(eff);

		fw.setFireworkMeta(fm);
	}

	public GameType getGameType() {
		return type;
	}
}
