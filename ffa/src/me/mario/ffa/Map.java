package me.mario.ffa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import me.mario.ffa.Game.GameType;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;


public class Map {

	private String name;
	private CuboidSelection selection;
	private Location lobbyloc;
	private static ArrayList<Map> maps = new ArrayList<Map>();
	
	public Map(String name, CuboidSelection selection, Location lobbyloc) {
		this.lobbyloc = lobbyloc;
		this.name = name;
		this.selection = selection;
		
		maps.add(this);
		
		System.out.println("Added map: " + name);
	}
	
	public Location getLobbyLoc() {
		return lobbyloc;
	}
	
	public Selection getSelection() {
		return selection;
	}
	
	public String getName() {
		return name;
	}
	
	public static Map getRandomMap() {
		Random r = new Random();
		
		return maps.get(r.nextInt(maps.size()));
	}

	public Location getRandomLocation(Player player, int Xminimum, int Xmaximum, int Zminimum, int Zmaximum, boolean tnt)
	{
		if(tnt) {
			World world = lobbyloc.getWorld();
			 
			int randomX = 0;
			int randomZ = 0;
			 
			double x = 0.0D;
			double y = 0.0D;
			double z = 0.0D;
			 
			randomX = Xminimum + (int)(Math.random() * (Xmaximum - Xminimum + 1)); //get random X
			randomZ = Zminimum + (int)(Math.random() * (Zmaximum - Zminimum + 1)); //get random Z
			 
			x = Double.parseDouble(Integer.toString(randomX));
			y = player.getLocation().getY() + 10D;
			z = Double.parseDouble(Integer.toString(randomZ));
			 
			x = x + 0.5; // add .5 so they spawn in the middle of the block
			z = z + 0.5;
			
			Location toReturn = new Location(world, x, y, z);
			
			return toReturn;
		}
		
	World world = lobbyloc.getWorld();
	 
	int randomX = 0;
	int randomZ = 0;
	 
	double x = 0.0D;
	double y = 0.0D;
	double z = 0.0D;
	 
	randomX = Xminimum + (int)(Math.random() * (Xmaximum - Xminimum + 1)); //get random X
	randomZ = Zminimum + (int)(Math.random() * (Zmaximum - Zminimum + 1)); //get random Z
	 
	x = Double.parseDouble(Integer.toString(randomX));
	y = Double.parseDouble(Integer.toString(world.getHighestBlockYAt(randomX, randomZ)));
	z = Double.parseDouble(Integer.toString(randomZ));
	 
	x = x + 0.5; // add .5 so they spawn in the middle of the block
	z = z + 0.5;
	
	Location toReturn = new Location(world, x, y, z);
	
	while(toReturn.getBlock().getType() == Material.LAVA || toReturn.getBlock().getType() == Material.STATIONARY_LAVA) {
		randomX = Xminimum + (int)(Math.random() * (Xmaximum - Xminimum + 1)); //get random X
		randomZ = Zminimum + (int)(Math.random() * (Zmaximum - Zminimum + 1)); //get random Z
		 
		x = Double.parseDouble(Integer.toString(randomX));
		y = Double.parseDouble(Integer.toString(world.getHighestBlockYAt(randomX, randomZ)));
		z = Double.parseDouble(Integer.toString(randomZ));
		 
		x = x + 0.5; // add .5 so they spawn in the middle of the block
		z = z + 0.5;
		
		toReturn = new Location(world, x, y, z);
	}
	
	return new Location(world, x, y, z);
	}

	public static ArrayList<Map> getMaps() {
		return maps;
	}

	public static GameType getRandomGameType() {
		List<GameType> gameTypes = Arrays.asList(GameType.values());
		Collections.shuffle(gameTypes);
		return gameTypes.get(0);
	}
	
}
