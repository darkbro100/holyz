package me.mario.ffa;

import java.util.ArrayList;

import me.mario.ffa.util.DBUtil;

import org.bukkit.Location;
import org.bukkit.SkullType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;

public class GameSign {

	public static ArrayList<GameSign> signs = new ArrayList<GameSign>();
	
	private SignType type;
	private Location loc;
	
	private String name;
	
	private int number;
	
	public GameSign(Location loc, SignType type, int number, String name) {
		this.loc = loc;
		this.type = type;
		this.number = number;
		this.name = name;

		System.out.println("New Game sign: " + name + " type: " + type.name() + " number: " + number);
		
		signs.add(this);
		
		a();
	}
	
	public int getNumber() {
		return number;
	}
	
	public SignType getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}
	
	public Location getLocation() {
		return loc;
	}
	
	@SuppressWarnings("deprecation")
	public void a() {
		GameSign sign = this;
		
		Block block = sign.getLocation().getBlock();
		if(block.getState() instanceof Sign) {
			
			Sign s = (Sign) block.getState();
			
			if(sign.getType() == SignType.BEST_KILLSTREAK) {
				s.setLine(0, "Top KILLSTREAK");
			} else if(sign.getType() == SignType.GAME_KILLS) {
				s.setLine(0, "Top GAME KILLS");
			} else if(sign.getType() == SignType.PLAYED) {
				s.setLine(0, "Top GAMES PLAYED");
			}  else if(sign.getType() == SignType.TIME_PLAYED) {
				s.setLine(0, "Top PLAY TIME");
			}else {
				s.setLine(0, "Top " + sign.getType().name());
			}
			
			s.setLine(1, "#" + (sign.getNumber() + 1));
			
			ArrayList<String> list = new ArrayList<String>();
			
			Main.db.getConnection();
			if(sign.getType() == SignType.ELO) {
				list = DBUtil.getTop10("elo");
			} else if(sign.getType() == SignType.KILLS) {
				list = DBUtil.getTop10("kills");
			} else if(sign.getType() == SignType.WINS) {
				list = DBUtil.getTop10("wins");
			} else if(sign.getType() == SignType.BEST_KILLSTREAK) {
				list = DBUtil.getTop10("bestKillstreak");
			} else if(sign.getType() == SignType.PLAYED) {
				list = DBUtil.getTop10("gamesPlayed");
			} else if(sign.getType() == SignType.GAME_KILLS) {
				list = DBUtil.getTop10("bestGameKills");
			} else if(sign.getType() == SignType.TIME_PLAYED) {
				list = DBUtil.getTop10("timePlayed");
			} else if(sign.getType() == SignType.KDR) {
				list = DBUtil.getBestKDR();
			}
			Main.db.closeConnection();
			
			String[] ar = list.get(sign.getNumber()).split(" ");
			
			String name = ar[0];
			
			s.setLine(2, name);
			
			String number = ar[2];
			if(sign.getType() == SignType.TIME_PLAYED) {
				number = Main.fh.convert(number);
			}
			
			if(sign.getType() == SignType.BEST_KILLSTREAK) {
				s.setLine(3, number + " " + "KILLSTREAK");
			} else if(sign.getType() == SignType.GAME_KILLS) {
				s.setLine(3, number + " " + "GAME KILLS");
			} else if(sign.getType() == SignType.TIME_PLAYED) {
				s.setLine(3, number + " " + "PLAYED");
			} else {
				s.setLine(3, number + " " + sign.getType().name());
			}
			s.update();
			
			Block block1 = null;
			
			int radius = 3;
			
            for (int x = -(radius); x <= radius; x ++){
                for (int y = -(radius); y <= radius; y ++) {
                  for (int z = -(radius); z <= radius; z ++) {
                      Location loc = sign.getLocation().getBlock().getRelative(x, y, z).getLocation();
                      if(loc.getBlock().getState() instanceof Skull) {
                    	  block1 = loc.getBlock();
                    	  break;
                      }
                  }
                }
              }
            
            if(block1 != null) {
    			
    			block1.setData((byte)0x1);
    			BlockState state = block1.getState();
    			 
    			if(state instanceof Skull)
    			{
    			    Skull skull = (Skull)state;
    			 
    			    skull.setSkullType(SkullType.PLAYER);
    			    skull.setOwner(name);
    			    
    			    skull.update();
    			    
    			}
            }
		}
	}
	
	public static void update() {
		for(GameSign sign : GameSign.signs) {
			sign.a();
		}
	}
}
