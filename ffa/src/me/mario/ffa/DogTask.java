package me.mario.ffa;

import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.EntityWolf;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWolf;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.scheduler.BukkitRunnable;

public class DogTask extends BukkitRunnable {

	public void run() {
		
		if(GamePlayer.players.size() > 0) {
			for(GamePlayer player : GamePlayer.players) {
				if(!player.hasdogs) continue;
				
				for(LivingEntity ent : player.dogs) {
					if(ent.getType() != EntityType.WOLF) continue;
					
					Entity p = getNearestPlayer(player);
					if(p == null) {
						CraftWolf cwolf = (CraftWolf) ent;
						cwolf.setOwner(player.getPlayer());
						cwolf.setOwnerUUID(player.getPlayer().getUniqueId());
						continue;
					}
					
					Player pl = (Player) p;
					
					EntityPlayer ep = ((CraftPlayer)pl).getHandle();
					
					EntityWolf wolf = ((CraftWolf)ent).getHandle();
					
					wolf.setGoalTarget(ep, TargetReason.CLOSEST_PLAYER, false);
				}
			}
		}
	}

	private Entity getNearestPlayer(GamePlayer loc) {
		double distance = 9999999999D;
		Entity toReturn = null;
		
		for(Entity ent : loc.getPlayer().getNearbyEntities(16D, 16D, 16D)) {
			if(ent instanceof Player) {
				GamePlayer gp = GamePlayer.get((Player)ent);
				
				if(!gp.isInGame()) continue;
				if(gp.getTeam() != null && loc.getTeam() != null) {
					if(gp.getTeam().name.equalsIgnoreCase(loc.getTeam().name)) continue;
				}
				
				if(ent.getLocation().distance(loc.getPlayer().getLocation()) <= distance) {
					distance = ent.getLocation().distance(loc.getPlayer().getLocation());
					toReturn = ent;
				}
			}
		}
		return toReturn;
	}
}
